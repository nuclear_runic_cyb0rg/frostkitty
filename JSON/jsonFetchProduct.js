import fetch from "node-fetch";


// * @param {string} shopname - Customer username
// * @param {string} icecat_id - product Icecat ID
// * @param {string} app_key - App key, can be found in the FO
// * @param {string} lang - Language, can be found at https://iceclog.com/icecat-locales-and-language-code-table/
// * @param {Array} granularParams - Granular parameters
// * @param {string} brand - product brand
// * @param {string} mpn - product model number
// TODO @param {string} gtin - product GTIN
// * @returns {string} - URL
function _jsonCreateLink(params) {
  const { 
    shopname,
    app_key,
    icecat_id,
    lang,
    granularParams,
    brand,
    mpn
  } = params;
  const DOMAIN = `https://live.icecat.biz/api?`


  let url =
    DOMAIN +`shopname=${shopname}` + `&lang=${lang.toUpperCase()}` + `&app_key=${app_key}`;
    
    if (icecat_id) url += `&icecat_id=${icecat_id}`
    if (brand && mpn) url += `&brand=${brand}&ProductCode=${mpn}`
    if (granularParams) url += "&content=" + granularParams.join(",")

  // Enable for debugging
  // console.log("Your link is: " + url)


  return url;

}

//TODO: May be adjusted to become a factory function for all requests
//TODO: Add error handling
//? Might be public
//* @param "string" url - URL to fetch
// get JSON for single product.
async function _fetchProduct(url) {
  const response = await fetch(url);
  if (!response.ok) {
    return "No product found";
  }
  const productData = await response.json();
  return productData;
}

//* @param {Object} productIdentifiers - Handled by jsonCreateLink
// PUBLIC FUNCTION, SHOULD BE CALLED FROM ANYWHERE
async function jsonFetchProduct(productIdentifiers) {
  const url = _jsonCreateLink(productIdentifiers);
  let product = await _fetchProduct(url);
  return product;
}



export default jsonFetchProduct
