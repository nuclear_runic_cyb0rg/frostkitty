import { jsonGetProductByMPN } from "./icecatJSON/liveJSONFetcher.js";
import fs from "fs";

const JSON_DATA = fs.readFileSync("./assets/reviewsImportData.json", "utf8");
const inputJSONData = JSON.parse(JSON_DATA);


const inputProductData = {
    shopname: "icecat-qa-valera-shop",
    app_key: "E8kxsx2Mu5mipNrZ54i6sH28HCEB5o2G",
    lang: "en",
    granularParams: ['related'],
    mpn: "",
    brand: ""
}

async function createRelatedProductsArray() {

    const relatedProductData = []

    let numberOfProductsCompleted = 0

    for (const product of inputJSONData) {
        // Console log to see progress
        // Just to make sure we don't get stuck in an infinite loop
        console.log(`Working on ${product.Icecatpartcode}/${product.Icecatvendor}...`)
        numberOfProductsCompleted++
        if (numberOfProductsCompleted % 100 === 0) {
            console.log(`Completed ${numberOfProductsCompleted} products`)
        }

        // Get the product data
        inputProductData['mpn'] = product.Icecatpartcode
        inputProductData['brand'] = product.Icecatvendor
        // Common fields for all products
        let relatedProductDataObject = {
            MPN: product.Icecatpartcode,
            Brand: product.Icecatvendor
        }
        const productData = await jsonGetProductByMPN(inputProductData);
        if (productData === "No product found") {
            relatedProductData.push(relatedProductDataObject)
            continue
        }


        //* Example of a related product array
        // Client requested BRAND and MPN fields for each related product
        /**
         *  ProductRelated: [
                {
                ID: 295676201,
                CategoryID: 1675,
                Preferred: 0,
                IcecatID: 88653793,
                ProductCode: 'CTE351SRUBK',
                ThumbPic: '',
      //*       ProductName: 'CT-E351',
      //*       Brand: 'Citizen',
                BrandID: 1006,
                ProductRelatedLocales: [Array]
                }]
         */

        const relatedProducts = productData.data.ProductRelated
        
        let numberOfRelatedProducts = 0
        for (const relatedProduct of relatedProducts) {
            numberOfRelatedProducts++
            const fieldNameBrand = "ProductRelated" + numberOfRelatedProducts + "Brand"
            const fieldNameMPN = "ProductRelated" + numberOfRelatedProducts + "MPN"
            
            relatedProductDataObject[fieldNameBrand] =  relatedProduct.Brand,
            relatedProductDataObject[fieldNameMPN] =  relatedProduct.ProductCode
            
        }

        // Enable to see the progress of each product
        // console.log(relatedProductDataObject)
        relatedProductData.push(relatedProductDataObject)

        // Timeout must be less than 100 requests per second
        // Otherwise the API will return a 429 error
        // I'll set it to 0.33 second just to be safe
        await new Promise(resolve => setTimeout(resolve, 333));

    }
    console.log(relatedProductData)
    return relatedProductData
}

async function main() {
        const relatedProductData = await createRelatedProductsArray();
        const jsonData = JSON.stringify(relatedProductData, null, 2);
        fs.writeFileSync("./assets/relatedProducts.json", jsonData);

}
main().catch(err => console.log(err));