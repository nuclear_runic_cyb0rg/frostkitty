import path from 'path';
import fs from "fs"
import splitXML from './js/splitXML.js';
import countProductsInXML from './js/countProductsInXML.js';

// const smallInputFileName = path.join("vertical.index.files", "1965.vertical.files.index.xml")
// const largeInputFileName = path.join("vertical.index.files", "2833.vertical.files.index.xml")

// splitXML(largeInputFileName, 1000000);

function getXmlFilePaths(folderPath) {
    try {
      const files = fs.readdirSync(folderPath);
      return files
        .filter(file => path.extname(file).toLowerCase() === '.xml')
        .map(file => path.join(folderPath, file));
    } catch (error) {
      console.error('Error reading folder:', error);
      return [];
    }
  }
  
  // Usage example:
  const folderPath = 'vertical.index.files';
  const xmlFilePaths = getXmlFilePaths(folderPath);
  console.log('List of XML files:', xmlFilePaths);

  for (let i of xmlFilePaths) {
    countProductsInXML(i)
  }