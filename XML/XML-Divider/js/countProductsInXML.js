import fs from "fs"
import sax from "sax"
import path from "path"

// Function to create CSV data from XML
function countProductsInXML(xmlFilePath) {
  // Create a SAX stream for parsing XML
  const saxStream = sax.createStream(true);

  // Variable to keep track of file count
  let fileCount= 0;

  // Event listener for handling XML tags
  saxStream.on("opentag", (tag) => {
    // Check if the current tag is a "file"
    if (tag.name === "file") {
      // Extract attributes from the "file" tag
      fileCount++;
    }
  });

  // Event listener for the end of the XML stream
  saxStream.on("end", () => {
    // Check if there's any remaining data after parsing
    console.log(`${xmlFilePath}: ${fileCount}`)
    fileCount = 0;
  });

  // Read the XML file and pipe it to the SAX stream for parsing
  fs.createReadStream(xmlFilePath).pipe(saxStream);
}

export default countProductsInXML