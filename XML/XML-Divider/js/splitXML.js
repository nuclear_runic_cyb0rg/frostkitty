import fs from "fs"
import sax from "sax"
import path from "path"

// Function to create CSV data from XML
function splitXML(xmlFilePath, chunkSize) {
  // Create a SAX stream for parsing XML
  const saxStream = sax.createStream(true);

  // Initialize CSV data with column titles
  let csvData = 'path,Limited,HighPic,HighPicSize,HighPicWidth,HighPicHeight,Product_ID,Updated,Quality,Prod_ID,Supplier_id,Catid,On_Market,Product_View,Date_Added\n';

  // Variable to keep track of file count
  let filesProcessed = 0;
  let fileCountInChunk = 0;

  // Event listener for handling XML tags
  saxStream.on("opentag", (tag) => {
    // Check if the current tag is a "file"
    if (tag.name === "file") {
      // Extract attributes from the "file" tag
      const {
        path: filePath,
        Limited,
        HighPic,
        HighPicSize,
        HighPicWidth,
        HighPicHeight,
        Product_ID,
        Updated,
        Quality,
        Prod_ID,
        Supplier_id,
        Catid,
        On_Market,
        Product_View,
        Date_Added
      } = tag.attributes;

      // Append the extracted data to the CSV string
      csvData += `${filePath},"${Limited}","${HighPic}","${HighPicSize}","${HighPicWidth}","${HighPicHeight}","${Product_ID}","${Updated}","${Quality}","${Prod_ID}","${Supplier_id}","${Catid}","${On_Market}","${Product_View}","${Date_Added}"\n`;

      fileCountInChunk++;
      if (fileCountInChunk % 20000 === 0) {
        console.log(fileCountInChunk + " files processed!")
      }

      // Check if the chunk size is reached
      if (fileCountInChunk === chunkSize) {
        filesProcessed++;
        // Write the CSV data to a file named 'output_{filesProcessed}.csv'

        fs.writeFileSync(
          path.join('computers_and_peripherals',`ComputersAndPeripherals_${filesProcessed}.csv`), 
          csvData
          );
        console.log(`CSV file ${filesProcessed} created successfully!`);

        // Reset variables for the next chunk
        fileCountInChunk = 0;
        csvData = 'path,Limited,HighPic,HighPicSize,HighPicWidth,HighPicHeight,Product_ID,Updated,Quality,Prod_ID,Supplier_id,Catid,On_Market,Product_View,Date_Added\n';
      }
    }
  });

  // Event listener for the end of the XML stream
  saxStream.on("end", () => {
    // Check if there's any remaining data after parsing
    if (fileCountInChunk > 0) {
      // Write the remaining CSV data to a file
      fs.writeFileSync(`output_${filesProcessed}.csv`, csvData);
      console.log(`CSV file ${filesProcessed} created successfully!`);
    }
  });

  // Read the XML file and pipe it to the SAX stream for parsing
  fs.createReadStream(xmlFilePath).pipe(saxStream);
}

export default splitXML