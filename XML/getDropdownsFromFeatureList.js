import sax from 'sax';

//* FeaturesList.xml structure
{/* <FeaturesList>
	<Feature Class="0" DefaultDisplayUnit="0" ID="5" Type="numerical" Updated="2008-09-25 12:25:49">
		<Descriptions>
			<Description ID="577544" langid="0" Updated="2018-10-30 14:47:54">The clock rate is the fundamental rate in cycles per second (measured in hertz) at which a computer performs its most basic operations such as adding two numbers. There are more factors</Description>
			<Description ID="579264" langid="1" Updated="2018-10-30 14:47:54">The clock rate is the fundamental rate in cycles per second (measured in hertz) at which a computer performs its most basic operations such as adding two numbers. There are more factors, like internal memory size, that influence a computer's actual response time.</Description>
		</Descriptions>
		<Measure ID="18" Sign="MHz" Updated="2008-03-09 17:19:09">
			<Signs>
				<Sign ID="53806" langid="71" Updated="2021-07-19 10:43:18">MHz</Sign>
				<Sign ID="53525" langid="72" Updated="2021-07-19 10:43:18">MHz</Sign>
			</Signs>
		</Measure>
		<Names>
			<Name ID="7674521" langid="73" Updated="2014-12-03 10:36:12">Processor frequency</Name>
			<Name ID="7879696" langid="74" Updated="2014-12-03 10:36:12">Frequenza del processore</Name>
		</Names>
		<RestrictedValues ValueSorting="0">
			<RestrictedValue/>
		</RestrictedValues>
	</Feature>
</FeaturesList> */}

//TODO Replace with fetch 
let path = './ref_files/FeaturesList.xml'

let insideFeature = false
let insideRestrictedValue = false
let parser = sax.createStream(true); // strict mode

parser.on('error', (err) => {
    console.error('XML parsing error:', err);
});

parser.on('opentag', (node) => {

    if (node.name === "Feature") {
        insideFeature = true
        let type = node.attributes["Type"]
        // console.log(type)

        if (type === 'dropdown' || type === 'multi_dropdown') {
            // console.log(type + " found!")
            // console.log(node.attributes["ID"])
        }
    }
    else if (insideFeature && node.name === "RestrictedValue") {
        insideRestrictedValue = true
    }

    // Process each XML node as it's being parsed
    // console.log('Open tag:', node.name);
});

parser.on('text', (text) => {
    if(insideRestrictedValue){
        // console.log(text)
        if (text.includes('\u00A0')) console.log('Feature with nonbreakable space!')
    }
})

parser.on('closetag', (nodeName) => {
    if (nodeName == "Feature") {
        insideFeature = false
    }
    if (nodeName == "RestrictedValue") {
        insideRestrictedValue = false
    }
    // Process closing tags
    // console.log('Close tag:', nodeName);
});

// Create a read stream from the XML file
let xmlFileStream = createReadStream(path);

// Pipe the XML file stream to the SAX parser
xmlFileStream.pipe(parser);